// console.log ("Hello World");

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: [
		"Pikachu", 
		"Charizard", 
		"Squirtle", 
		"Bulbasaur"
	],
	friends: {
		hoenn: [
			"May",
			"Max"
		],
		kanto: [
			"Brock",
			"Misty"
		]
	},
	talk: function() {
		console.log("Pikachu! I choose you!");
	}
}

console.log(trainer);

// Dot Notation
console.log("Result from dot notation:");
console.log(trainer.name);

// Square Bracket Notation
console.log("Result from square bracket notation:");
console.log(trainer['pokemon']);

// Talk Method
console.log("Result of talk method");
trainer.talk();

function Pokemon(name, level) {
	// Properties 
	this.name = name;
	this.level = level;
	this.health = 2 * level; 
	this.attack = level;

	// Methods/Function
	this.tackle = function(target) {
		this.target = target.name;
		let healthAfterAttack = (target.level *2) - this.level;
		console.log(this.name + " tackled " + this.target);
		console.log(this.target + "'s" + " health is now reduced to" + " " + healthAfterAttack);
	};

	this.faint = function(target) {
		this.target = target.name;
		let healthAfterAttack = (target.level *2) - this.level;

		if (this.health <= 0) {
			console.log(this.target + " fainted");

			let newAttribute = new Pokemon(this.target, this.health);
			console.log(newAttribute);
		}else {
			let newAttribute = new Pokemon(this.target, this.level);
			console.log(newAttribute);
		};
	}
	
	/*this.faint = function (target) {
		this.target = target.name;
		// this.newHealth = target.level -this.level 
		let healthAfterAttack = (target.level *2) - this.newHealth;
		
		// let newPokemonAttribute = new Pokemon(this.target, this.newHealth);
		if(healthAfterAttack <= 0) {
			console.log(this.target + " fainted");
			// console.log(newPokemonAttribute);
		}else {
			// console.log(newPokemonAttribute);
		};
	};*/
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);
geodude.faint(pikachu);
mewtwo.tackle(geodude);
mewtwo.faint(geodude);